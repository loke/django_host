from django.urls import path

from .views import HomepageView, hello_world_view

app_name = 'consumer'

urlpatterns = [
    path('hello', hello_world_view, name='hello'),
    path('', HomepageView.as_view(), name='home')
]
