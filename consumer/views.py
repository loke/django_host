from django.http.response import HttpResponse
from django.views.generic import TemplateView


class HomepageView(TemplateView):
    template_name = 'home.html'


def hello_world_view(request):
    return HttpResponse('Hello, World!')
